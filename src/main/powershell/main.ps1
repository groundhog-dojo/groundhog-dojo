#!/usr/bin/env pwsh

# Entry point for the program. This function will be called
# by the tool to test the implementation once the time expires.
# If the test fails, all the changes made to this file will be
# reset.

