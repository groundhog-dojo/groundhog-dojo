% -*- erlang -*-

-module(main).
-export([main/1]).

%% Entry point for the application. This function will be called
%% by the script to test the implementation once the time expires.
%% If the test fails, all the changes made to this file will be
%% reset.
main(Args) -> ok.

