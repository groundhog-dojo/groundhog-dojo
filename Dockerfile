FROM ubuntu:latest
RUN useradd -U -l -d /gd -s /bin/bash gd
COPY --chown=gd:gd . /gd
RUN DEBIAN_FRONTEND="noninteractive" TZ="Europe/London" apt-get update && apt-get -y install tzdata
RUN apt-get update && apt-get -y install \
 git \
 curl \
 make \
 tmux \
 nano \
 vim-nox \
 emacs-nox
RUN apt-get update && apt-get -y install \
 openjdk-17-jdk-headless \
 python3-nox \
 erlang-nox \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*
USER gd
WORKDIR /gd
ENTRYPOINT ["./gd.sh"]
