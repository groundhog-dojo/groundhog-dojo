#!/usr/bin/env bash

set -e

if ! which docker; then
    echo "Docker not available. Exiting!" >&2
    exit 1
fi

OUT=/dev/stdout

if [[ ${1} == "-q" ]]; then
    OUT=/dev/null
    shift
fi

SUFFIX=${1:+-${1}}
DOCKERFILE="Dockerfile.web${SUFFIX}"; shift
PORT_OPTS=${PORT:+-p ${PORT}:8080}

docker build -f "${DOCKERFILE}" -t groundhog-dojo . > ${OUT}
docker run --name groundhog-dojo${SUFFIX} -dt --rm ${PORT_OPTS} groundhog-dojo "${@}"
