#!/usr/bin/java --source 11 -Xmx128m

import java.io.*;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

class gdd {

    private static final Logger logger = Logger.getLogger("gdd");
    private static final int DEFAULT_PORT = 2020;

    public static void main(String[] args) {
        new gdd().run();
    }

    private void run() {
        try (var serverSocket = new ServerSocket(DEFAULT_PORT)) {
            while (!Thread.interrupted()) {
                try (var socket = serverSocket.accept()) {
                    var meta = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    var classpath = meta.readLine();
                    var classname = meta.readLine();
                    var stdin = meta.readLine();
                    var stdout = meta.readLine();
                    var stderr = meta.readLine();
                    var argnumber = Integer.parseInt(meta.readLine());
                    String[] args = new String[argnumber];
                    for (int i = 0; i < argnumber; i++) {
                        args[i] = meta.readLine();
                    }

                    try (var in = new LazyInputStream(Path.of(stdin), socket);
                         var out = Files.newOutputStream(Path.of(stdout));
                         var err = Files.newOutputStream(Path.of(stderr))) {
                        new ClassRunner(classpath, classname, args, in, out, err).run();
                    } catch (Exception e) {
                        logger.log(Level.SEVERE, e.getMessage(), e);
                    }
                } catch (Exception e) {
                    logger.log(Level.SEVERE, e.getMessage(), e);
                }
            }
        } catch (Exception e) {
            System.err.printf("ERROR: %s%n", e.getMessage());
        }
    }

    static class ClassRunner implements Runnable {

        private final String classPath;
        private final String className;
        private final String[] args;

        private final InputStream in;
        private final PrintStream out;
        private final PrintStream err;

        private InputStream oldIn;
        private PrintStream oldOut;
        private PrintStream oldErr;

        public ClassRunner(String classPath, String className, String[] args,
                           InputStream in, OutputStream out, OutputStream err) {
            this.classPath = classPath;
            this.className = className;
            this.args = args;
            this.in = in;
            this.out = new PrintStream(out);
            this.err = new PrintStream(err);
        }

        @Override
        public synchronized void run() {
            try {
                List<URL> list = new ArrayList<>();
                for (String s : classPath.split(":")) {
                    list.add(Path.of(s).toUri().toURL());
                }
                var urls = list.toArray(new URL[0]);
                var cl = new URLClassLoader(urls) {
                    final Class<?> loadedClass = findClass(className);
                };
                captureStreams();
                try (cl) {
                    Method main = cl.loadedClass.getMethod("main", String[].class);
                    main.setAccessible(true);
                    MethodHandles.lookup()
                            .unreflect(main)
                            .withVarargs(true)
                            .invokeWithArguments(List.of(args));
                } catch (NoSuchMethodException e) {
                    err.printf("Error: No main method found in class: %s%n", className);
                } catch (Throwable e) {
                    e.printStackTrace(err);
                } finally {
                    resetStreams();
                }
            } catch (ClassNotFoundException e) {
                err.printf("Error: Could not find or load main class %s%n", className);
                err.printf("Caused by: java.lang.ClassNotFoundException: %s%n", className);
            } catch (Exception e) {
                logger.log(Level.SEVERE, String.format("Failed to run class: %s:%s", classPath, className), e);
            }
        }

        private void captureStreams() {
            oldIn = System.in;
            oldOut = System.out;
            oldErr = System.err;
            System.setIn(in);
            System.setOut(out);
            System.setErr(err);
        }

        private void resetStreams() {
            System.setIn(oldIn);
            System.setOut(oldOut);
            System.setErr(oldErr);
        }

    }

    private static class LazyInputStream extends InputStream {

        enum Command {
            OPEN('O'), READ('R'), CLOSE('C');
            private final char code;
            Command(char c) { code = c; }
        }

        private final Path source;
        private final Socket socket;
        private InputStream stream;

        public LazyInputStream(Path source, Socket socket) {
            this.source = source;
            this.socket = socket;
        }

        private InputStream stream() throws IOException {
            if (stream == null) {
                cmd(Command.OPEN);
                stream = new BufferedInputStream(new FileInputStream(source.toFile()));
            }
            return stream;
        }

        private void cmd(Command cmd) throws IOException {
            socket.getOutputStream().write(cmd.code);
        }

        @Override
        public int read() throws IOException {
            if (available() == 0) {
                cmd(Command.READ);
            }
            return stream().read();
        }

        @Override
        public int available() throws IOException {
            return stream().available();
        }

        @Override
        public void close() throws IOException {
            try (socket) {
                cmd(Command.CLOSE);
            }
            if (stream != null) {
                stream.close();
            }
        }

    }

}
