#!/usr/bin/env bash

source ${GD_ROOT}/lib/tests.sh

input:  ""
output: 0
---
input:  "Why did the Star Wars episodes 4, 5 and 6 come before 1, 2 and 3? Because in charge of planning, Yoda was."
output: 6
---
input:  "22:36:44"
output: 3
---
input:  "99 percent of all statistics only tell 49 percent of the story."
output: 2
---
input:  "String without numbers"
output: 0
---
summary
