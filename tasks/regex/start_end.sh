#!/usr/bin/env bash

source ${GD_ROOT}/lib/tests.sh

input:  "\\d+" "Why did the Star Wars episodes 4, 5 and 6 come before 1, 2 and 3? Because in charge of planning, Yoda was."
output: << END
start: 31, end: 32
start: 34, end: 35
start: 40, end: 41
start: 54, end: 55
start: 57, end: 58
start: 63, end: 64
END
---
input: "genius" "The difference between stupidity and genius is that genius has its limits."
output: << END
start: 37, end: 43
start: 52, end: 58
END
---
input: "" ""
output: << END
start: 0, end: 0
END
---
input: "genius" ""
output: << END
END
---
summary
