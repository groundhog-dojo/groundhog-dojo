# Join words

Write a program which, when given a list of strings as arguments, prints them joined with a colon (':').

Example:
```
$ run foo bar baz
foo:bar:baz
```
