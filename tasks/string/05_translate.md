# Translate

Write a program which, when given three strings as arguments, prints the third one with each character from the first one replaced with a corresponding character from the second one.

Example:
```
$ run "ABLI" "XYYX" "ALIBABA"
XYXYXYX
3
```
