# Count words

Write a program which, when given a string as a single argument, prints the number of substrings in that string, separated by exactly one space (" ").

Example:
```
$ run "foo bar baz"
3
```
