# Reverse

Write a program which, when given a string as argument, prints its reversed form.

Example:
```
$ run "quack"
kcauq

```
