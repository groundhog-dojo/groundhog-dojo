# Average of numbers

Write a program which, when given a list of numbers as arguments, prints the average of all numbers in the list.

Example:
```
$ run 5 4 6 3 7 2
3.0
```
