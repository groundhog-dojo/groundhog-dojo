# Convert hexadecimal to decimal 

Write a program which, when given a hexadecimal number as argument, prints the number in the decimal form.

Example:
```
$ run CAFEBABE
3405691582
```
