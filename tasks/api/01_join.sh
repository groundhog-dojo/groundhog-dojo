#!/usr/bin/env bash

source ${GD_ROOT}/lib/tests.sh

input: 
output: 
---
input:  a
output: a
---
input:  a b
output: ab
---
input:  a b c
output: abc
---
input:  a b c d
output: abcd
---
input:  a b c d e f
output: abcdef
---
summary
