# Bubble Sort

Write a program which, when given an array of integers, returns the numbers sorted ascendingly using Bubble Sort algorithm.

Bubble Sort consists of N rounds, in each of which bigger elements in the not-yet-sorted part of the array move towards the end by being repetitively swapped with the smaller ones. This way, the largest element becomes the last one, and the not-yet-sorted part of the array decreases.

Example:
```
$ java 8 1 6 3 4 5 2 7
1 2 3 4 5 6 7 8
```
