# Count '3'

Write a program which, when given a number as an argument, prints the number of occurences of digit 3 in that number.

Example:
```
$ run 3030303
4
```
