# Palindrome

Write a program which, when given a string as an argument, prints "true", when the word is a palindrome, and "false" otherwise.

Palindrome is a word which is spelled the same forwards and backwards.

Example:
```
$ run "madamimadam"
true
```
