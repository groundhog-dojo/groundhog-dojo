# Sum of even numbers

Write a program which, when given a list of numbers as arguments, prints the sum of all even numbers in the list.

Even number is a number divisible by 2.

Example:
```
$ run 5 4 6 3 7 2
12
```
