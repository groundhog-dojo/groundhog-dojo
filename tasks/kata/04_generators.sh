#!/usr/bin/env bash

source ${GD_ROOT}/lib/tests.sh

input:  2
output: 1
---
input:  3
output: 2
---
input:  5
output: 2 3
---
input:  7
output: 3 5
---
input:  11
output: 2 6 7 8
---
input:  13
output: 2 6 7 11
---
summary
