# Generators

Write a program which, for a given prime number P, prints all generators of the multiplicative group of integers modulo P.

Generator is such a number from 1 to P-1 which, multiplied by itself (mod P) exactly P-1 times, gives 1.

Example:
```
$ run 13
2 6 7 11
```
