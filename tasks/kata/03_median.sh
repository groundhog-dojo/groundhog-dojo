#!/usr/bin/env bash

source ${GD_ROOT}/lib/tests.sh

input:  5 4 6 3 1 2
assert: 'bc <<< "${OUTPUT} > 3 && ${OUTPUT} < 4"' 1
---
summary
