# Find number 3

Write a program which, when given a list of numbers as arguments, prints the index (starting with 0) of the number 3 in the list, or -1, if there's no such number.

Example:
```
$ run 5 4 6 3 7 2
3
```
