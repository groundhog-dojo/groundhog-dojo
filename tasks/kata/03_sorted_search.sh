#!/usr/bin/env bash

source ${GD_ROOT}/lib/tests.sh

input:             
output: -1
---
input:  5           
output: -1
---
input:  3           
output: 0
---
input:  3 5         
output: 0
---
input:  5 3         
output: 1
---
input:  5 4 6 3 7 2 
output: 3
---
summary
