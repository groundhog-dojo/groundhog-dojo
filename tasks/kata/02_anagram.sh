#!/usr/bin/env bash

source ${GD_ROOT}/lib/tests.sh

input: "" ""
output: true
---
input: "TOMMARVOLORIDDLE" "IAMLORDVOLDEMORT"
output: true
---
input: "ELVIS" "LIVES"
output: true
---
input: "FUNERAL" "REALFUN"
output: true
---
input: "ELEVEN PLUS TWO" "TWELVE PLUS ONE"
output: true
---
summary
