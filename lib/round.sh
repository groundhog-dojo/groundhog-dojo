#!/bin/bash

source ${GD_ROOT}/lib/util.sh
source ${GD_ROOT}/lib/stat.sh

task=${1:?"No task provided"}
timeout=${2:-300}
step=${3:-30}

export GD_TASK="${GD_TASKS}/${task}"

while true; do

    cleanup "${task}"
   
    elapsed=$(date +%s)

    kill_session="tmux wait-for -S $$; tmux kill-pane -a -t 0"
    editor="${GD_EDIT_COMMAND}; ${kill_session}"
    timer="${GD_ROOT}/lib/countdown.sh ${timeout}; ${kill_session}"
    shell="/usr/bin/env bash; ${kill_session}"
    instr="trap '' INT; ${GD_ROOT}/lib/mdfmt.sed ${GD_TASK}.md | fold -w 45 -s | less -R~"   
    hints="trap '' INT; tail -f ${GD_EDITORS}/${GD_EDITOR}/help"

    tmux split-window -h -t 0 -d "${timer}"
    tmux split-window -v -t 0 \
        -e GD_MAKE_COMMAND="${GD_MAKE_COMMAND}" \
        -e GD_TEST_COMMAND="${GD_TEST_COMMAND}" \
        -e GD_TASK="${GD_TASK}" \
        -d "${editor}"
    tmux split-window -v -t 2 -d "${instr}"
    tmux split-window -v -t 3 -d "${hints}"
    tmux split-window -v -t 1 \
        -e GD_MAKE_COMMAND="${GD_MAKE_COMMAND}" \
        -e GD_TEST_COMMAND="${GD_TEST_COMMAND}" \
        -e GD_TASK="${GD_TASK}" \
        -d "${shell}"

    tmux select-pane -t 1 -T 'Editor'
    tmux select-pane -t 2 -T 'Shell'
    tmux select-pane -t 3 -T 'Timer' -d
    tmux select-pane -t 4 -T 'Task'  -d
    tmux select-pane -t 5 -T 'Help'  -d

    tmux resizep -t 0 -y 1 \; resizep -t 2 -y 10 \; resizep -t 3 -x 45 -y 8 \; resizep -t 5 -y 10

#    tmux set-hook -a client-attached 'resizep -t 0 -y 1 ; resizep -t 2 -y 10 ; resizep -t 3 -x 45 -y 8 ; resizep -t 5 -y 10'
    tmux set-hook -a client-resized  'resizep -t 0 -y 1 ; resizep -t 2 -y 10 ; resizep -t 3 -x 45 -y 8 ; resizep -t 5 -y 10'

    tmux select-pane -t 1

    echo -n "${task}"
    tmux wait-for $$
    echo

    if make compile test; then
        success=true
        ((timeout -= ${step}))
    else
        success=false
        ((timeout += ${step}))
    fi
    
    elapsed=$(($(date +%s) - elapsed))
    
    log_stats "${task}" "${elapsed}" "${success}"
    print_stats "${task}"

    ask "Try again?" || break
    clear
done

cleanup master

tmux kill-window
