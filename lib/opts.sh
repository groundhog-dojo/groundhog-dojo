# -*- bash -*-

source ${GD_ROOT}/lib/util.sh

declare -A OPTS
OPTS=(
    [h]=help
    [e]=env
    [E]=editor
    [l]=list-tasks
    [C]=no-cleanup
    [R]=randomize
    [s]=step
    [t]=timeout
)

declare -A OPT_METADATA
OPT_METADATA=(
    [help]=,opt_help,"print this help and exit."
    [env]=:,opt_env,"set environment (java)."
    [editor]=:,opt_editor,"set editor (vim)."
    [list-tasks]=,opt_list_tasks,"list all tasks within a given group."
    [no-cleanup]=,opt_no_cleanup,"disable cleanup at each new turn."
    [randomize]=,opt_randomize,"randomize the order of tasks."
    [step]=:,opt_step,"set number of seconds by which the timeout is decreased/increased  on success/failure."
    [timeout]=:,opt_timeout,"set fixed amount of time for the execution for all tasks."
)

short_opts() {
    local -n opts=$1
    local opt fields
    opts=""
    for opt in ${!OPTS[@]}; do
        read_opt ${opt} fields
        opts="${opts}${opt}${fields[0]}"
    done
}

long_opts() {
    local -n opts=$1
    local opt fields
    opts=""
    for opt in ${OPTS[@]}; do
        read_opt ${opt} fields
        opts="${opts}${opt}${fields[0]},"
    done
    opts="${opts%,}"
}

read_opt() {
    local opt=${OPTS[${1}]:-${1}}
    local val="${OPT_METADATA[${opt}]}"
    [[ -n ${val} ]] && IFS="," read -a "${2}" <<< "${val}"
}

parse_opts() {
    local SHORT_OPTS LONG_OPTS

    short_opts SHORT_OPTS
    long_opts LONG_OPTS

    ARGS=$(getopt -o ${SHORT_OPTS} --long ${LONG_OPTS} -- "$@" || exit 2)
    eval set -- "$ARGS"
    
    while true ; do
        shopt -s extglob
        local opt=${1/#?(-|--)}
        shopt -u extglob

        if read_opt ${opt} opt_metadata; then
            if [[ -n ${opt_metadata[0]} ]]; then
                local arg="${2}"
                shift;
            fi
            ${opt_metadata[1]} "${arg}"
            shift;
        elif [[ ${1} = "--" ]]; then
            shift;
            break;
        else
            die "Error while parsing args!"
        fi
    done

    ARGS="$@"
}

opt_list_tasks() {
    while [[ ${1} != "--" ]]; do shift; done
    list_tasks ${2}
    exit 0
}

opt_env() {
    GD_ENV="${1}"
}

opt_editor() {
    GD_EDITOR="${1}"
}

opt_no_cleanup() {
    GD_CLEANUP=false
}

opt_randomize() {
    GD_SORT_OPTS="-R"
}

opt_timeout() {
    if [[ ${1} =~ [+-][0-9]* ]]; then
        GD_TIMEOUT_MOD="${1}"
    else
        GD_TIMEOUT="${1}"
    fi
}

opt_step() {
    GD_STEP="${1}"
}

opt_help() {
    local sopt lopt opt_metadata
    echo -e "Usage: ${0} [options] [task | task_group]"
    for sopt in ${!OPTS[@]}; do
        lopt=${OPTS[${sopt}]}
        read_opt ${sopt} opt_metadata
        echo -e "  -${sopt}${opt_metadata[0]:+ <arg>}, --${lopt}${opt_metadata:+=<arg>}"
        echo -e "\t${opt_metadata[2]}"
    done
    exit 0
}
