#!/usr/bin/env bash

[[ $0 = */bash ]] && BASE="${PWD}" || BASE="$(dirname $0)"

source ${BASE}/util.sh
source ${BASE}/fonts/${GD_FONT:-arabic}.sh

countdown() {
    local -i eta color=7
    for (( eta = ${1} ; eta >= 0 ; eta-- )); do
        clear_screen

        if [[ ${eta} -le 10 ]]; then
            color=1
        elif [[ ${eta} -le 30 ]]; then
            color=3
        fi

        local sep tmp
        [[ $((${eta} % 2)) != 0 ]] && sep=" " || sep=":"
        ftime -s "${sep}" ${eta} tmp
        clock "${tmp}" ${color}

        echo -en "\e[0m"
        sleep 1
    done
}

clear_screen() {
    echo -en "\e[2J\e[1;1H"
}

clock() {
    local -i i j
    local time="$1" color="${2}"
    local c=$((30+${color:-7}))
    printf "\e[${c}m"
    for (( i = 0 ; i < ${GD_FONT_HEIGHT} ; i++ )); do
        local ln=''
        for (( j = 0 ; j < ${#time} ; j++ )); do
            local ch=${time:j:1}
            case "${ch}" in
                [0-9]) local -n tab="__${ch}__" ;;
                  ":") local -n tab="__COLON__" ;;
                  " ") local -n tab="__SPACE__" ;;
            esac
            ln+=" ${tab[${i}]}"
        done
        printf "${ln}\n"
    done
    printf "\e[0m"
}

countdown ${1:?"Usage: countdown <time_in_seconds>"}

